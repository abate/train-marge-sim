import random
import argparse


class MergeRequest:
    def __init__(self, id, title, tests_passed, merge_conflicts):
        self.id = id
        self.title = title
        self.tests_passed = tests_passed
        self.merge_conflicts = merge_conflicts
        self.ttm = 1
        self.merged = False

    def time_to_merge(self, t):
        if t != 0:
            self.ttm = t
            self.merged = True


class MergeTrain:
    def __init__(self):
        self.train = []

    def merge_rebase(self, merge_request):
        return random.random() <= merge_request.merge_conflicts

    def add_to_train(self, merge_request):
        # The MR is added to the train if it does not have
        # merge conflicts
        if self.merge_rebase(merge_request):
            self.train.append(merge_request)
            return True
        else:
            return False

    def merge(self, merge_request):
        # The MR is merged if all tests pass
        # otherwise is removed from the train and all other
        # MRs must be rebased and tested again
        if random.random() <= merge_request.tests_passed:
            return True
        return False


class MergeBot:
    def __init__(self):
        self.merge_requests = []

    def monitor_merge_requests(self, merge_request):
        self.merge_requests.append(merge_request)

    def review_and_merge(self, merge_request):
        # Simulate automated checks
        # A MR can fail both because by the time Marge
        # tried to merge it there is a merge conflict,
        # or because the pipeline fails.

        if (
            random.random() <= merge_request.tests_passed
            and random.random() <= merge_request.merge_conflicts
        ):
            return True
        return False


def run_gitlab_merge_trains_simulation(
    num_merge_requests, success_rate, conflicts_rate, num_simulations
):
    total_time_to_merge = 0
    total_wasted_ci_time = 0
    total_number_of_mrs = 0
    for _ in range(num_simulations):
        merge_train = MergeTrain()

        # Ordering and Chaining MRs
        wasted_ci_time = 0
        for i in range(1, num_merge_requests + 1):
            title = f"Feature #{i}"
            tests_passed = random.uniform(success_rate, 1.0)
            merge_conflicts = random.uniform(conflicts_rate, 1.0)
            merge_request = MergeRequest(i, title, tests_passed, merge_conflicts)
            # if the rebase fail, we do not add it on the queue.
            # a rebase can fail because the pipeline of the previous
            # MR actually fails. This is wasted ci time.
            # the ci time wasted depends on how many MRs are
            # going to be queued in the meantime on the train
            if not merge_train.add_to_train(merge_request):
                wasted_ci_time += 1

        number_of_mrs = 0
        # we consider all MRs in the merge train and we merge them
        # one after the other
        vagons = len(merge_train.train)
        failed = 0  # number of MR removed from the train
        removed_from_train = False
        for i, mr in enumerate(merge_train.train):
            # if the previous MR was removed from the train
            if removed_from_train:
                failed += 1
                # if the MR can be rebased
                if merge_train.merge_rebase(mr):
                    # if this MR can be merged
                    if merge_train.merge(mr):
                        # the time to merge depends on the number
                        # of MR before that were removed from the train
                        # if they were all successful them time to merge is 1
                        mr.time_to_merge(failed)
                        number_of_mrs += 1
                        removed_from_train = False
                    else:
                        # if the ci for an MR failed, then all MRs queued after
                        # must be re-run
                        wasted_ci_time += vagons - i
                        removed_from_train = True
                # else: if the MR cannot be rebase,
                # the MR is removed from the train
            else:
                # previous MR was sucessful
                if merge_train.merge(mr):
                    # If the CI for this MR was succesful

                    # the time to merge depends on the number
                    # of MR before that were removed from the train
                    # this is basically how many time I had to re-run the ci
                    mr.time_to_merge(failed)
                    number_of_mrs += 1
                    removed_from_train = False
                else:
                    # if the ci for an MR failed, then all MRs queued after
                    # must be re-run
                    wasted_ci_time += vagons - i
                    removed_from_train = True

        # larger train, larger risk

        # ci time * rate failore * train size

        total_time_to_merge += (
            sum(mr.ttm for mr in merge_train.train if mr.merged) / number_of_mrs
        )
        total_wasted_ci_time += wasted_ci_time
        total_number_of_mrs += number_of_mrs

    avg_time_to_merge = total_time_to_merge / num_simulations
    avg_wasted_ci_time = total_wasted_ci_time / num_simulations
    avg_number_of_mrs = total_number_of_mrs / num_simulations
    return (avg_time_to_merge, avg_wasted_ci_time, avg_number_of_mrs)


def run_merge_bot_simulation(
    num_merge_requests, success_rate, conflicts_rate, num_simulations
):
    total_time_to_merge = 0
    total_wasted_ci_time = 0
    total_number_of_mrs = 0
    for _ in range(num_simulations):
        merge_bot = MergeBot()

        for i in range(1, num_merge_requests + 1):
            title = f"Feature #{i}"
            tests_passed = random.uniform(success_rate, 1.0)
            merge_conflicts = random.uniform(conflicts_rate, 1.0)
            merge_request = MergeRequest(i, title, tests_passed, merge_conflicts)
            # The MR cannot be assigned to marge if there is a
            # rebase conflict, so no time wasted here.
            merge_bot.monitor_merge_requests(merge_request)

        number_of_mrs = 0
        wasted_ci_time = 0
        for i, mr in enumerate(merge_bot.merge_requests):
            if merge_bot.review_and_merge(mr):
                # time for a MR to be merged depends on the position on the queue
                mr.time_to_merge(i)
                number_of_mrs += 1
            else:
                # if this MR cannot be merged, we have wasted only
                # the time for this MR, since marge works sequentially.
                wasted_ci_time += 1

        total_time_to_merge += (
            sum(mr.ttm for mr in merge_bot.merge_requests if mr.merged) / number_of_mrs
        )
        total_number_of_mrs += number_of_mrs
        total_wasted_ci_time += wasted_ci_time

    avg_time_to_merge = total_time_to_merge / num_simulations
    avg_wasted_ci_time = total_wasted_ci_time / num_simulations
    avg_number_of_mrs = total_number_of_mrs / num_simulations
    return (avg_time_to_merge, avg_wasted_ci_time, avg_number_of_mrs)


def main():
    parser = argparse.ArgumentParser(
        description="Run simulations for GitLab Merge Trains and Merge Bot."
    )

    parser.add_argument(
        "--num-merge-requests", type=int, default=500, help="Number of merge requests"
    )
    parser.add_argument(
        "--success-rate",
        type=float,
        default=0.99,
        help="Success rate for the CI 0: all failed / 1: all success",
    )
    parser.add_argument(
        "--conflicts-rate",
        type=float,
        default=0.9,
        help="Success rate of merge rebase 0: all failed / 1: all success",
    )
    parser.add_argument(
        "--num-simulations", type=int, default=100, help="Number of simulations"
    )

    args = parser.parse_args()

    (
        gitlab_avg_time_to_merge,
        gitlab_avg_wasted_ci_time,
        gitlab_number_of_mrs,
    ) = run_gitlab_merge_trains_simulation(
        args.num_merge_requests,
        args.success_rate,
        args.conflicts_rate,
        args.num_simulations,
    )
    (
        merge_bot_avg_time_to_merge,
        marge_avg_wasted_ci_time,
        marge_number_of_mrs,
    ) = run_merge_bot_simulation(
        args.num_merge_requests,
        args.success_rate,
        args.conflicts_rate,
        args.num_simulations,
    )

    print(f"Run {args.num_simulations} simulations")
    print(f"Conflict rate: {args.conflicts_rate}")
    print(f"Success rate: {args.success_rate}")
    print(f"Number of merge requests: {args.num_merge_requests}")
    print("\n")
    print("GitLab Merge Trains:")
    print(f" - Average Time to Merge: {gitlab_avg_time_to_merge}")
    print(f" - Wasted CI time: {gitlab_avg_wasted_ci_time}")
    print(f" - Total number of merged MRs: {gitlab_number_of_mrs}")

    print("Merge Bot:")
    print(f" - Average Time to Merge: {merge_bot_avg_time_to_merge}")
    print(f" - Wasted CI time: {marge_avg_wasted_ci_time}")
    print(f" - Total number of merged MRs: {marge_number_of_mrs}")


if __name__ == "__main__":
    main()
