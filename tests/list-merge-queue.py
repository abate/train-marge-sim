import os
import time
import random
import argparse
from gitlab import Gitlab, exceptions
from git import Repo, GitCommandError
from decouple import config, UndefinedValueError


def authenticate_gitlab():
    # Load environment variables from .env file with default values
    GITLAB_URL = config("GITLAB_URL", default="https://gitlab.com")
    try:
        GITLAB_PRIVATE_TOKEN = config("GITLAB_PRIVATE_TOKEN")
    except UndefinedValueError as e:
        raise EnvironmentError(f"Required environment variable is not defined: {e}")

    gl = Gitlab(GITLAB_URL, private_token=GITLAB_PRIVATE_TOKEN)
    gl.auth()

    return gl


def add_to_merge_train(gl, project, mr):
    print(f"Adding MR {mr.iid} to merge train")
    try:
        # print(gl.http_get(f"/projects/{project.id}/merge_trains"))
        endpoint = f"/projects/{project.id}/merge_trains/merge_requests/{mr.iid}" # ?when_pipeline_succeeds=true"
        print(endpoint)
        gl.http_post(endpoint)
        time.sleep(5)
    except exceptions.GitlabHttpError as e:
        print(f"HTTP Error: {e}")
    except exceptions.GitlabParsingError as e:
        print(e)


def checking_mr_train(gl, project):
    print("Checking merge trains for master")
    merge_trains = project.merge_trains.list(
        scope="active"  # , target_branch=mr.target_branch
    )
    for t in merge_trains:
        print(f"!{t.merge_request['iid']} on the train for master")

def fetch_mrs_assigned_or_labeled(gl, project):

    mrs = project.mergerequests.list(state='opened', iterator=True, target_branch="master")

    mrs = [mr for mr in mrs if
            ((mr.assignee and mr.assignee["username"] == "nomadic-margebot") or
            "MR_IS_READY" in mr.labels ) and not mr.draft ]

    for t in mrs:
        print(f"!{t.iid} Ready to be merged")
        print(f"{t.title} / {t.web_url}")
        print(f"Assignee: {t.assignee['username']} \n")

def main():
    parser = argparse.ArgumentParser(
        description="Create Merge Requests with conflicts for GitLab project"
    )
    parser.add_argument("--project", help="GitLab project name", required=True)

    subparsers = parser.add_subparsers(dest="command", help="Available subcommands")

    # Train subcommand
    train_parser = subparsers.add_parser("train", help="Query the merge train")

    # MRs subcommand
    mrs_parser = subparsers.add_parser("mrs", help="Fetch Merge Requests")

    args = parser.parse_args()

    project_name = args.project
    gl = authenticate_gitlab()
    project = gl.projects.get(project_name)

    if args.command == "train":
        checking_mr_train(gl, project)
    elif args.command == "mrs":
        fetch_mrs_assigned_or_labeled(gl, project)
    else:
        print("Please provide a valid subcommand. Use '--help' for more information.")

if __name__ == "__main__":
    main()
