import os
import time
import random
import argparse
from gitlab import Gitlab, exceptions
from git import Repo, GitCommandError
from decouple import config, UndefinedValueError


def authenticate_gitlab(repo_path):
    # Load environment variables from .env file with default values
    GITLAB_URL = config("GITLAB_URL", default="https://gitlab.com")
    try:
        GITLAB_PRIVATE_TOKEN = config("GITLAB_PRIVATE_TOKEN")
    except UndefinedValueError as e:
        raise EnvironmentError(f"Required environment variable is not defined: {e}")

    gl = Gitlab(GITLAB_URL, private_token=GITLAB_PRIVATE_TOKEN)
    gl.auth()

    repo = Repo(repo_path)

    # Set the GitLab remote URL with the private token if "gitlab-ci-token" is not present
    gitlab_remote_url = repo.remotes.origin.url
    if "gitlab-ci-token" not in gitlab_remote_url:
        gitlab_remote_url = gitlab_remote_url.replace(
            "://", f"://gitlab-ci-token:{GITLAB_PRIVATE_TOKEN}@"
        )
        print(f"Remote URL updated: {gitlab_remote_url}")
        repo.remotes.origin.set_url(gitlab_remote_url)
    else:
        print("Remote URL already contains 'gitlab-ci-token'")

    return (gl, repo)


def clone_project(project_name):
    repo_path = f"./{project_name}"
    if os.path.exists(repo_path):
        print(f"Repository '{project_name}' already exists. Updating...")
        repo = Repo(repo_path)

        try:
            # Ensure we are on the master branch
            repo.git.checkout("master")

            # Check if the repository is clean
            is_repo_clean = not repo.is_dirty()

            # Reset the repository to discard any local changes
            if not is_repo_clean:
                repo.git.reset("--hard", "origin/master")

            # Pull the latest changes from the remote master branch
            repo.remotes.origin.pull()
        except GitCommandError as e:
            print(f"Error during update: {e}")
    else:
        print(f"Cloning repository '{project_name}'...")
        gl = authenticate_gitlab()
        project = gl.projects.get(project_name)
        repo_url = project.http_url_to_repo
        Repo.clone_from(repo_url, repo_path)

    return repo_path


def commit_and_push_changes(
    repo, repo_path, branch_name, changes, experiment_number, mr_number, dry_run
):
    # Create a random directory for each experiment
    experiment_dir = f"experiment_{experiment_number}"
    experiment_path = os.path.join(repo_path, experiment_dir)
    os.makedirs(experiment_path, exist_ok=True)

    print("Checkout a fresh master as target branch")
    repo.git.reset("--hard")
    repo.git.checkout("master")
    repo.git.pull("origin", "master")

    repo.git.checkout("-b", branch_name)
    os.makedirs(experiment_path, exist_ok=True)

    # Make changes and commit
    for change in changes:
        action = change["action"]
        file_full_path = os.path.join(experiment_path, change["file_path"])
        content = change.get("content", "")

        if action == "create":
            with open(file_full_path, "w") as file:
                file.write(content)
        elif action == "delete":
            if os.path.exists(file_full_path):
                os.remove(file_full_path)
        elif action == "update":
            with open(file_full_path, "w") as file:
                file.write(content)

    repo.git.add(".")
    repo.git.commit("-m", f"Commit changes for branch {branch_name}")
    print(f"Commit changes locally {branch_name}")

    if not dry_run:
        print(f"Push changes remotely {branch_name}")
        repo.git.push("origin", branch_name)


def create_mr(project, source_branch, target_branch, title, changes):
    mr = project.mergerequests.create(
        {
            "source_branch": source_branch,
            "target_branch": target_branch,
            "title": title,
            "changes": changes,
            "labels": ["MR_IS_NOT_READY"]
        }
    )

    # Wait until the MR is correctly created
    while True:
        mr = project.mergerequests.get(mr.iid)
        if mr.state == "opened" and mr.head_pipeline:
            print(f"MR !{mr.iid} is successfully created.")
            break
        elif mr.state == "closed":
            print("MR creation failed. The MR is closed.")
            break
        else:
            # Introduce another small delay before checking again
            time.sleep(1)

    return mr


def trigger_manual_pipeline(project, mr, success_rate):
    # Set CI environment variable based on success_rate
    job_name = "trigger"
    pipeline = project.pipelines.get(mr.head_pipeline["id"])

    job = None
    for pipeline_job in pipeline.jobs.list():
        j = project.jobs.get(pipeline_job.id)
        if j.name == job_name:
            job = j
            break

    if job:
        # Trigger the manual job
        # It's not possible to pass CI variable via the gitlab python lib
        # https://github.com/python-gitlab/python-gitlab/issues/663
        # ci_variables = {"CI_SUCCESS_RATE": str(success_rate)}
        job.play()
        print(f"Manual job '{job_name}' in pipeline {pipeline.id} has been triggered.")
    else:
        print(f"Job '{job_name}' not found in pipeline {pipeline.id}.")


def add_to_merge_train(gl, project, mr):
    print(f"Adding MR {mr.iid} to merge train")
    try:
        # print(gl.http_get(f"/projects/{project.id}/merge_trains"))
        endpoint = f"/projects/{project.id}/merge_trains/merge_requests/{mr.iid}" # ?when_pipeline_succeeds=true"
        print(endpoint)
        gl.http_post(endpoint)
        time.sleep(5)
    except exceptions.GitlabHttpError as e:
        print(f"HTTP Error: {e}")
    except exceptions.GitlabParsingError as e:
        print(e)


def checking_mr_train(gl, project):
    print("Checking merge trains for master")
    merge_trains = project.merge_trains.list(
        scope="active"  # , target_branch=mr.target_branch
    )
    for t in merge_trains:
        print(f"!{t.merge_request['iid']} on the train for master")
    # print(gl.http_get(f"/projects/{project.id}/merge_trains?scope=active"))
    # if len(merge_trains) == 0:
        # print("Empty merge train")
        # mr.merge(merge_when_pipeline_succeeds=True)
        # time.sleep(5)
        # print(gl.http_get(f"/projects/{project.id}/merge_trains?scope=active"))
    # else:


def main():
    parser = argparse.ArgumentParser(
        description="Create Merge Requests with conflicts for GitLab project"
    )
    parser.add_argument("--project", help="GitLab project name", required=True)
    parser.add_argument(
        "--conflict-rate", help="Conflict rate (0 to 1)", type=float, required=True
    )
    parser.add_argument(
        "--success-rate", help="Success rate (0 to 1)", type=float, required=True
    )
    parser.add_argument(
        "--dry-run",
        help="Perform a dry run without pushing changes",
        action="store_true",
    )
    parser.add_argument(
        "--merge-train",
        help="Add the MR to the merge train when the pipeline succeeds",
        action="store_true",
    )
    parser.add_argument(
        "--merge",
        help="Set to merge the MR when the pipeline succeeds",
        action="store_true",
    )
    parser.add_argument(
        "--trigger-pipeline",
        help="Trigger the manual pipeline",
        action="store_true",
    )



    parser.add_argument("n", type=int, help="Number of Merge Requests to create")

    args = parser.parse_args()

    project_name = args.project
    conflict_rate = args.conflict_rate
    success_rate = args.success_rate
    n = args.n
    dry_run = args.dry_run
    experiment_number = random.randint(1, 1000)

    print(f"Experiment number {experiment_number}")
    repo_path = clone_project(project_name)
    (gl, repo) = authenticate_gitlab(repo_path)
    project = gl.projects.get(project_name)

    mr_list = []
    for mr_number in range(1, n + 1):
        source_branch = f"experiment_{experiment_number}_branch_{mr_number}"
        target_branch = "master"
        title = f"Merge Request {mr_number} / experiment {experiment_number}"

        # Add, remove, and modify some files for changes
        changes = [
            {
                "action": "create",
                "file_path": f"file_{mr_number}.txt",
                "content": "New File Content",
            },
            {"action": "delete", "file_path": f"file_{mr_number - 1}.txt"},
            # {
            #     "action": "update",
            #     "file_path": f"file_{mr_number - 2}.txt",
            #     "content": "Modified File Content",
            # },
        ]

        # Introduce conflicts based on conflict_rate
        if random.random() < conflict_rate:
            conflicting_file = f"file_{random.randint(0, mr_number - 1)}.txt"
            changes.append(
                {
                    "action": "update",
                    "file_path": conflicting_file,
                    "content": "Conflicting Change",
                }
            )

        commit_and_push_changes(
            repo,
            repo_path,
            source_branch,
            changes,
            experiment_number,
            mr_number,
            dry_run,
        )

        if not dry_run:
            # Create Merge Request
            mr = create_mr(project, source_branch, target_branch, title, changes)
            mr_list.append(mr)

        # determine_success_failure:
        #   script:
        #     - echo "Determining success or failure based on CI_SUCCESS_RATE..."
        #     - if [[ $(awk 'BEGIN{print ENVIRON["CI_SUCCESS_RATE"] >= rand()}') == "1" ]]; then exit 0; else exit 1; fi

        print(f"Merge Request '{title}' created successfully.")

    if not dry_run and args.merge:
        for mr in mr_list:
            # We give gitlab time to breath between requests
            time.sleep(1)
            print(f"Set !{mr.id} to be merged with pipeline succeed")
            mr.merge(merge_when_pipeline_succeeds=True)
            # https://github.com/python-gitlab/python-gitlab/issues/2547
            # apparently we cannot add the MR to the merge train via the
            # gitlab python library

    if not dry_run and args.trigger_pipeline:
        for mr in mr_list:
            trigger_manual_pipeline(project, mr, success_rate)

    if not dry_run and args.merge_train:
        for mr in mr_list:
            # We give gitlab time to breath between requests
            time.sleep(1)
            print(f"Set !{mr.iid} be added to a merge train")
            add_to_merge_train(gl, project, mr)
            # https://github.com/python-gitlab/python-gitlab/issues/2547
            # apparently we cannot add the MR to the merge train via the
            # gitlab python library

    print("Leave a few secs to gitlab to think")
    time.sleep(15)
    checking_mr_train(gl, project)


if __name__ == "__main__":
    main()
