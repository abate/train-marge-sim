import argparse
import pandas as pd


def calculate_rates(filename):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(
        filename,
        usecols=["Time", "MR", "Reason"],
        engine="python",
    )

    # Filter out lines with "insufficient_approvals" and "forbidden_commit_titles"
    df = df[~df["Reason"].isin(["insufficient_approvals", "forbidden_commit_titles"])]

    # Categorize reasons based on specified criteria
    df["Result"] = df["Reason"].apply(
        lambda x: "success"
        if x == "undefined"
        else ("rebase_conflict" if x == "rebase_conflicts" else "failure")
    )

    rebase_conflicts = df[df["Result"] == "rebase_conflict"]

    failure_rate = len(df[df["Result"] == "failure"]) / len(df) * 100
    success_rate = len(df[df["Result"] == "success"]) / len(df) * 100
    rebase_conflict_rate = len(rebase_conflicts) / len(df) * 100

    print(f"Average Failure Rate: {failure_rate:.2f}%")
    print(f"Average Success Rate: {success_rate:.2f}%")
    print(f"Average Rebase Conflict Rate: {rebase_conflict_rate:.2f}%")


if __name__ == "__main__":
    # Set up command-line argument parsing
    parser = argparse.ArgumentParser(
        description="Calculate average failure rate and rebase conflict rate from a CSV file."
    )
    parser.add_argument("filename", help="Path to the CSV file")

    # Parse command-line arguments
    args = parser.parse_args()

    # Call the function with the provided filename
    calculate_rates(args.filename)
