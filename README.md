
A simplistic simulation of margebot vs merge trains

```
poetry install

poetry run python sim.py
```

ex:

```
Run 20 simulations
GitLab Merge Trains:
 - average Time to Merge: 40.25
 - wasted ci time 9.75
 - total number of merged MRs 40.25
Merge Bot:
 - average Time to Merge: 35.5
 - total number of merged MRs 38
```
